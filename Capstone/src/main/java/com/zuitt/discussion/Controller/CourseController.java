package com.zuitt.discussion.Controller;

import com.zuitt.discussion.Service.CourseService;
import com.zuitt.discussion.models.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseController {
    @Autowired
    CourseService courseService;

    // Create Post
    //@PostMapping("/posts")
    @RequestMapping(value = "/course",method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course){
        courseService.createCourse(stringToken,course);
        return new ResponseEntity<>("Course created successfully", HttpStatus.CREATED);
    }

    // Get all posts
    @GetMapping("/course")
    public ResponseEntity<Object> getPost(){
        return new ResponseEntity<>(courseService.getPosts(), HttpStatus.OK);
    }

    // Delete a post
    @RequestMapping(value = "/course/{courseid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long courseid,@RequestHeader(value="Authorization") String stringToken){
        return courseService.deletePost(stringToken,courseid);
    }

    //    Update a post
    @RequestMapping(value = "/course/{courseid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatedPost(@PathVariable Long courseid,@RequestHeader(value="Authorization")String stringToken, @RequestBody Course course){
        return courseService.updatePost(courseid,stringToken, course);
    }
    //  Get user post
    /*@GetMapping("/myPost")
    public ResponseEntity<Object> getMyPost(@RequestHeader(value="Authorization")String stringToken){
        return new ResponseEntity<>(courseService.getMyPosts(stringToken),HttpStatus.OK);*/
}
